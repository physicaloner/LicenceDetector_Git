﻿// เรียกใช้เนมสเปช
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO;
using System.Diagnostics;

using Emgu.CV;
using Emgu.CV.Structure;
using System.Timers;
using System.ComponentModel;
using Win = System.Windows.Forms;
using System.Windows.Threading;
using System.Windows.Interop;
using System.Runtime.InteropServices;
using Point = System.Drawing.Point;
using Emgu.CV.Features2D;
using Emgu.CV.CvEnum;

// กำหนดชื่อเนมสเปช
namespace LicenceDetector
{
    // กำหนดชื่อคลาส : ประเภทของคลาส
    public partial class MainWindow : Window
    {
        System.Threading.Thread bgListVideo = null;  // เธรดอัพเดทรายชื่อวิดีโอ
        System.Threading.Thread bgPythonScript = null; // เธรดเรียกใช้คำสั่งไพธอน
        System.Threading.Thread bgUpdateFrame = null; // เธรดเล่นวิดีโอทีละเฟรม

        bool firstState = false, IsCrossed = false; // ตัวแปรเช็คสถานะว่ารันครั้งแรกหรือไม่ , ตัวแปรคอยเล่นเฟรมเลขคี่ (ถ้าจะเล่นเฟรมเลขคู่ปรับเป็น IsCrossed = true)

        int playerIndex = 0; // ตัวแปรกำหนดว่าจะเล่นวิดีโอ play list ที่เท่าไหร่
        string fileIndex = string.Empty; // ตัวแปรชื่อไฟล์วิดีโอที่จะเล่น
        int LowH, LowS, LowV, HighH, HighS, HighV, stepRoad = 1; // ตัวแปรค่า HSV low และ HSV high , ตัวแปรกำหนดกรอบถนน
        double getLEDX, getLEDY, getRoadX1, getRoadY1, getRoadX2, getRoadY2; // ตัวแปรเก็บค่ากรอบไฟแดง xy , ตัวแปรเก็บค่ากรอบเส้นถนน xy1 - xy2
        double VidX, VidY; // ตัวแปรเก็บขนาดวิดีโอ

        private int frameCount = 0; // frame count
        private bool foundLED = false, hasCapture = false; // พบสีแดงหรือไม่ ?

        DispatcherTimer tmr5000 = new DispatcherTimer(); // ตัวแปรจับเวลาให้ทำงานวนลูปทุก ๆ 5 วินาที

        Capture capture; // ตัวแปรเก็บค่าประมวลผลวิดีโอ (OpenCV)
        bool isMediaEnded = false; // ตัวแปรเก็บค่าสถานะวิดีโอว่าเล่นจบหรือยัง (จบแล้ว = true , เล่นอยู่ = false)
        uint videoFrameCount = 0; // ตัวแปรนับว่าวิดีโอมีกี่เฟรม
        uint videoFrameIndex = 0; // ตัวแปรบอกว่าเฟรมปัจจุบันอยู่เฟรมที่เท่าไหร่

        // อีเว้นทำงานเมื่อโหลดคลาส
        public MainWindow()
        {
            InitializeComponent(); // ติดตั้งคอมโพแนนท์ของฟอร์ม (ปุ่มกดต่าง ๆ , วัดถุในฟอร์ม)
        }

        // อีเว้นทำงานเมื่อโหลดฟอร์ม
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            tmr5000.Tick += Tmr5000_Tick; // กำหนดอีเว้นการวนลูป ในชื่อ  Tmr5000_Tick
            tmr5000.Interval = new TimeSpan(0, 0, 0, 5, 0); // กำหนดให้ลูปทุก ๆ 5 วินาที (วัน, ชั่วโมง, นาที, วินาที, มิลลิวินาที)
            tmr5000.Start(); // ให้เริ่มการวนลูป

            bgPythonScript = new System.Threading.Thread(RunScriptPython); // กำหนดเธรดรันสคริป ให้ทำงานฟังชั่น RunScript แบบเบื้องหลัง
            bgPythonScript.Start(); // เริ่มการทำงานเธรด

            AddLog("กรุณาเลือกที่อยู่วิดีโอ..."); // แสดงข้อความใน Log ว่า "กรุณาเลือกที่อยู่วิดีโอ..."           

            // ถ้าไม่มีไฟล์ config.tmp ในโฟลเดอร์ script
            if (!File.Exists(Environment.CurrentDirectory + @"\script\config.tmp"))
            {
                // กำหนดค่าตัวแปรพื้นฐานของ HSV
                HighH = 255;
                HighS = 255;
                HighV = 255;
                scollHighH.Value = 255;
                scollHighS.Value = 255;
                scollHighV.Value = 255;
            }
            // แต่ถ้ามีไฟล์ config.tmp ในโฟลเดอร์ script
            else
            {
                // กำหนดขนาดบัฟเฟอร์ให้มีขนาดเท่ากับไฟล์ config.tmp ในโฟลเดอร์ script
                byte[] data = File.ReadAllBytes(Environment.CurrentDirectory + @"\script\config.tmp");

                LowH = BitConverter.ToInt32(data, 0); // อ่านค่าจากบิตที่ 0
                LowS = BitConverter.ToInt32(data, 4); // อ่านค่าจากบิตที่ 4
                LowV = BitConverter.ToInt32(data, 8); // อ่านค่าจากบิตที่ 8
                HighH = BitConverter.ToInt32(data, 12); // อ่านค่าจากบิตที่ 12
                HighS = BitConverter.ToInt32(data, 16); // อ่านค่าจากบิตที่ 16
                HighV = BitConverter.ToInt32(data, 20); // อ่านค่าจากบิตที่ 20
                getLEDX = BitConverter.ToDouble(data, 24);  // อ่านค่าจากบิตที่ 24
                getLEDY = BitConverter.ToDouble(data, 32);  // อ่านค่าจากบิตที่ 32
                getRoadX1 = BitConverter.ToDouble(data, 40); // อ่านค่าจากบิตที่ 40
                getRoadY1 = BitConverter.ToDouble(data, 48); // อ่านค่าจากบิตที่ 48
                getRoadX2 = BitConverter.ToDouble(data, 56); // อ่านค่าจากบิตที่ 56
                getRoadY2 = BitConverter.ToDouble(data, 64); // อ่านค่าจากบิตที่ 64

                // กำหนดค่าให้ scoll bar
                scollLowH.Value = LowH;
                scollLowS.Value = LowS;
                scollLowV.Value = LowV;
                scollHighH.Value = HighH;
                scollHighS.Value = HighS;
                scollHighV.Value = HighV;
            }
        }

        // อีเว้นทำงานเมื่อ Tmr5000 ครบตามเวลาที่กำหนดใน new Timespan(วัน, ชั่วโมง, นาที, วินาที, มิลลิวินาที)
        private void Tmr5000_Tick(object sender, EventArgs e)
        {
            Debug.WriteLine("Tick : {0}", 5000); // แสดงข้อความใน Debug ว่า Tick 5000

            // ถ้าวิดีโอเล่นจบแล้ว
            if (isMediaEnded)
            {
                // กำหนดให้ size = จำนวนวิดีโอทั้งหมด
                int size = 0;
                listMedia.Dispatcher.Invoke((Action)(() => size = listMedia.Items.Count));
                Debug.WriteLine("Size : " + size); // แสดงข้อความใน Debug ว่ามีวิดีโอทั้งหมดกี่ไฟล์

                // ถ้าวิดีโอมากกว่า 0 และถ้าไฟล์ที่เล่นอยู่ในปัจจุบันไม่ใช่ไฟล์สุดท้าย
                if (size > 0)
                    if (playerIndex < size - 1)
                    {
                        playerIndex++; // เลื่อนตำแหน่งวิดีโอ
                        // กำหนดให้ชื่อวิดีโอที่เล่นปัจจุบันเป็นไปตามตำแหน่งปัจจุบัน
                        listMedia.Dispatcher.Invoke((Action)(() => fileIndex = listMedia.Items[playerIndex].ToString()));
                        // กำหนดให้ txtPath = ตำแหน่งปัจจุบันด้วย                        
                        string path = "";
                        txtPath.Dispatcher.Invoke((Action)(() => path = txtPath.Text + "\\" + fileIndex));

                        // คืน Memory (Ram) ให้ OS
                        try
                        {
                            capture.Dispose();
                            capture = null;
                            GC.Collect();
                        }
                        catch (Exception) { }

                        // กำหนดตัวเล่นวิดีโอตาม path ที่ตั้งค่าไว้จากข้างต้น
                        capture = new Capture(path);
                        // กำหนดค่าตัวแปรขนาด กว้าง , สูง ของวิดีโอ
                        VidX = capture.Width;
                        VidY = capture.Height;
                        // สร้างเธรดเล่นวิดีโอ
                        bgUpdateFrame = new System.Threading.Thread(CaptureUpdate);
                        // กำหนดตำแหน่งให้เล่นเฟรมแรก
                        videoFrameIndex = 0;
                        // กำหนดว่าวิดีโอนี้มีทั้งหมดกี่เฟรม
                        videoFrameCount = (uint)Math.Floor(capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_COUNT));
                        // กำหนดสถานะว่าวิดีโอกำลังเล่นอยู่
                        isMediaEnded = false;
                        // เริ่มการทำงาน Render วิดีโอ
                        bgUpdateFrame.Start();
                        // แสดงข้อความใน Debug ว่าวิดีโอนี้มีทั้งหมดกี่เฟรม
                        Debug.WriteLine("Video Frame : {0}", videoFrameCount);
                    }
            }
        }

        // อีเว้นทำงานเมื่อฟอร์มกำลังปิด
        private void Window_Closing(object sender, CancelEventArgs e)
        {

        }

        // อีเว้นทำงานเมื่อกดปุ่มปิดหน้าต่างนี้
        private void Window_Closed(object sender, EventArgs e)
        {
            // ปิดการทำงานของเธรด bgUpdateFrame 
            try
            {
                bgUpdateFrame.Abort();
            }
            catch (Exception) { }

            // ปิดการทำงานของเธรด bgListVideo
            try
            {
                bgListVideo.Abort();
            }
            catch (Exception) { }
        }

        private void CaptureUpdate()
        {
            // วนลูปเรื่อย ๆ ตราบใดที่ videoFrameIndex ยังน้อยกว่าจำนวนเฟรมทั้งหมดของวิดีโอ
            while (videoFrameIndex < videoFrameCount)
            {
                try
                {
                    // ถ้าตัวเล่นวิดีโอ = null หรือ พ้อยเตอร์ของวิดีโอในตัวแปร = 0 ให้ออกจากลูป
                    if (capture == null || capture.Ptr == IntPtr.Zero)
                        break;

                    // ถ้าเธรด bgUpdateFrame ถูกสั่งให้ปิด หรือ bgUpdateFrame = null ให้ออกจากลูป
                    if (!bgUpdateFrame.IsAlive || bgUpdateFrame == null)
                        break;

                    // กำหนดตัวแปรรูปภาพ ให้มีค่า = ดึงวิดีโอมาจาก capture ทีละเฟรม
                    Image<Bgr, byte> source = capture.QueryFrame();

                    // ถ้ารูปภาพไม่เท่ากับ null และพ้อยเตอร์ของรูปภาพไม่เท่ากับ 0
                    if (source != null && source.Ptr != IntPtr.Zero)
                    {
                        Stopwatch watch = Stopwatch.StartNew(); // กำหนดค่าตัวแปรเวลาให้เริ่มจับเวลา 

                        // คำสั่งค้นหารูปภาพที่ตรงกัน
                        // Image<Bgr, byte> image = CompareImage(source, surface, ref hasImage);

                        // สร้างตัวแปรให้จัดเก็บการประมวลผลรูปภาพ
                        Image<Bgr, byte> image = null;

                        // ถ้าเป็นเลขคี่
                        if (!IsCrossed)
                        {
                            // กำหนดค่าตัวแปร Image ให้ทำงานแบบผลัดผ่อนได้ (ยอมให้ข้ามไปทำงานบรรทัดอื่นก่อนที่จะโหลดตัวแปรเสร็จ)
                            Lazy<Image<Bgr, byte>> img = new Lazy<Image<Bgr, byte>>(() => DrawGridRed(source));
                            image = img.Value;

                            // ล็อคค่า image แม้ค่าผลัดของ image จะถูกเปลี่ยนโดยเธรดอื่นแล้วก็ตาม
                            lock (image)
                            {
                                // คำนวนเฟรมเส้นถนน
                                bool roadResult = Process(image);
                                // แสดงเฟรมวิดีโอที่ได้บน picMedia (บนหน้าจอ)
                                // picMedia.Dispatcher.BeginInvoke((Action)(() => picMedia.Source = ToBitmapSource(image)));
                                // ถ้าเจอรถในพื้นที่กรอบ และ ถ้าเจอไฟแดง
                                if (roadResult != false && foundLED == true)
                                {
                                    // ถ้าอยู่ในสถานะพร้อมถ่ายรูปได้
                                    if (!hasCapture)
                                    {
                                        // ถ่ายรูปเก็บไว้ในโฟลเดอร์ Image
                                        image.Bitmap.Save(Environment.CurrentDirectory + @"\Image\" + DateTime.Now.ToString("Dec (hh-MM-yyyy) HH.mm.ss") + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                                        hasCapture = true;
                                    }
                                }

                                // แสดงเวลาที่ใช้ทั้งหมดใน Debug
                                Debug.WriteLine("ใช้เวลาทั้งหมด : {0} milli sec.", watch.ElapsedMilliseconds);

                                // กำหนดให้ค่าเฟรมคู่คี่เป็นค่าตรงกันข้าม (ถ้าคู่ให้เป็นคี่ ถ้าคี่ให้เป็นคู่)
                                IsCrossed = !IsCrossed;
                            }
                        }
                        else
                        {
                            // กำหนดให้ค่าเฟรมคู่คี่เป็นค่าตรงกันข้าม (ถ้าคู่ให้เป็นคี่ ถ้าคี่ให้เป็นคู่)
                            IsCrossed = !IsCrossed;
                        }

                        // ถ้าเฟรมที่เล่นน้อยกว่าเฟรมทั้งหมด
                        if (videoFrameIndex < videoFrameCount)
                        {
                            Debug.WriteLine("Frame : {0}", videoFrameIndex);

                            videoFrameIndex += 2; // เพิ่มค่า (เล่นเฟรมคู่)

                            //capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_POS_FRAMES, videoFrameIndex);
                            /*int sleepTime = (int)capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FPS);
                            System.Threading.Thread.Sleep((int) 1000.0 / sleepTime);*/

                            // ถ้าเป็นเฟรมสุดท้าย ให้ออกจากลูป
                            if (videoFrameIndex >= videoFrameCount)
                            {
                                AddLog("เล่นวิดีโอไฟล์ " + listMedia.Items[playerIndex].ToString() + " เสร็จสิ้น.");
                                isMediaEnded = true;
                                break;
                            }
                        }
                        // ถ้าเฟรมที่เล่นมากกว่าเฟรมทั้งหมด ออกจากลูปโดยไม่มีเงื่อนไข
                        else
                        {
                            break;
                        }

                        watch.Stop();

                        // lblStatus.Dispatcher.BeginInvoke((Action)(() => lblStatus.Content = status + "[ ใช้เวลาประมวลผล " + watch.ElapsedMilliseconds + " ms. ]"));
                    }
                    // ถ้ารูปภาพ = null ให้ออกจากลูปทันทีและกำหนดสถานะว่าวิดีโอจบแล้ว
                    else
                    {
                        AddLog("เล่นวิดีโอไฟล์ " + listMedia.Items[playerIndex].ToString() + " เสร็จสิ้น.");
                        isMediaEnded = true;
                        break;
                    }
                }
                // ถ้าเออเร่อเนื่องจากพบค่า Null ของตัวแปร ให้บอกว่าเล่นวิดีโอเออเร่อและกำหนดสถานะว่าวิดีโอจบแล้วพร้อมออกจากลูป
                catch (NullReferenceException nrx)
                {
                    Debug.WriteLine("Video Play Error ! : " + nrx.Message);
                    AddLog("เล่นวิดีโอไฟล์ " + listMedia.Items[playerIndex].ToString() + " เสร็จสิ้น.");
                    isMediaEnded = true;
                    break;
                }
                // ถ้าเออเร่อเนื่องจากเงื่อนไขอื่น ๆ ให้ออกจากลูป
                catch (Exception ex)
                {
                    Debug.WriteLine("Video Play Error ! : " + ex.Message);
                    break;
                }
            }

            // คืน Ram ให้กับ OS
            capture.Dispose();
            capture = null;
            GC.Collect();
            // แสดงข้อความว่าวิดีโอเล่นจบแล้วใน Debug
            Debug.WriteLine("Video Interrupted : Video Ended.");
        }

        // อีเว้นทำงานเมื่อกดปุ่ม cmdBrowse (ปุ่มค้นหาไฟล์วิดีโอ)
        private void cmdBrowse_Click(object sender, RoutedEventArgs e)
        {
            // กำหนดตัวแปรสำหรับเปิดอ่านตำแหน่งโฟลเดอร์ โดยให้ทำงานแบบ IDisposable
            using (Win.FolderBrowserDialog openFile = new Win.FolderBrowserDialog())
            {
                // ถ้าเลือกโฟลเดอร์แล้วกดปุ่ม OK
                if (openFile.ShowDialog() == Win.DialogResult.OK)
                {
                    // ถ้าตัวแปร capture ไม่เท่ากับ null ให้คืน Ram ให้ OS ก่อน
                    if (capture != null)
                    {
                        capture.Dispose();
                        capture = null;
                        GC.Collect();
                    }

                    // กำหนดให้ข้อความใน txtPath เท่ากับโฟลเดอร์ที่เลือก
                    txtPath.Text = openFile.SelectedPath;
                    // กำหนดให้รีเซ็ต เริ่มเล่นวิดีโอตำแหน่งแรกเสมอ
                    playerIndex = 0;

                    // ถ้าเป็นการเล่นครั้งแรก 
                    if (!firstState)
                    {
                        // สร้างเธรดสำหรับอัพเดทชื่อไฟล์วิดีโอ
                        bgListVideo = new System.Threading.Thread(UpdateListMedia);
                        bgListVideo.Start();
                        // กำหนดตัวแปรว่า ครั้งต่อๆไปจะไม่ใช่การเล่นครั้งแรกอีก
                        firstState = true;
                    }

                    // เคลียร์วิดีโอบนหน้าจอ
                    picMedia.Source = null;
                    // กำหนดให้ข้อความในปุ่ม cmdProcess = Process
                    cmdProcess.Content = "Process";
                }
            }
        }

        // ฟังชั่นอัพเดทรายชื่อวิดีโอ
        private void UpdateListMedia()
        {
            // วนลูปแบบไม่มีสิ้นสุด
            while (true)
            {
                try
                {
                    // กำหนดนามสกุลที่จะให้สามารถนำมาเพิ่มใน play list ได้
                    string[] extesnsions = new string[] { ".mp4", ".wmp" };

                    // กำหนดให้ตัวแปร pathFile เท่ากับข้อความใน txtPath
                    string pathFile = "";
                    txtPath.Dispatcher.Invoke((Action)(() => pathFile = txtPath.Text));

                    // สร้างตัวแปร list สำหรับเก็บชื่อไฟล์ทั้งหมดที่อ่านได้
                    IEnumerable<string> myFiles = GetFiles(pathFile, extesnsions, SearchOption.AllDirectories);

                    // เคลียร์ list วิดีโอเก่าออก
                    listMedia.Dispatcher.BeginInvoke((Action)(() => listMedia.Items.Clear()));

                    // กำหนดตัวแปร จำนวนวิดีโอที่อ่านได้
                    int count = 0;
                    foreach (var item in myFiles)
                    {
                        // แยกคำในข้อความออก โดยใช้ \ เป็นตัวแบ่งคำ
                        string[] fileItems = item.Split('\\');
                        // เพิ่มชื่อวิดีโอลงใน play list
                        listMedia.Dispatcher.BeginInvoke((Action)(() => listMedia.Items.Add(fileItems[fileItems.Length - 1])));
                        // แสดงชื่อวิดีโอที่เพิ่มใน Debug
                        Debug.WriteLine(fileItems[fileItems.Length - 1]);
                        // เพิ่มขนาดจำนวนวิดีโอที่อ่านได้
                        count++;
                    }

                    // แสดงจำนวนวิดีโอที่อ่านได้ทั้งหมด
                    Debug.WriteLine("Count : " + count);

                    // ถ้าวิดีโอที่อ่านได้จากโฟลเดอร์มีมากกว่า 0
                    if (count > 0)
                    {
                        // กำหนดให้ listMedia โฟกัสรายชื่อวิดีโอไปที่ตำแหน่งวิดีโอที่กำลังเล่นอยู่
                        listMedia.Dispatcher.BeginInvoke((Action)(() => listMedia.SelectedIndex = playerIndex));
                        // กำหนดให้ชื่อวิดีโอที่กำลังเล่นอยู่เท่ากับชื่อของวิดีโอของตำแหน่งปัจจุบัน
                        listMedia.Dispatcher.BeginInvoke((Action)(() => fileIndex = listMedia.SelectedItem.ToString()));
                    }
                    // ถ้าวิดีโอที่อ่านได้จากโฟลเดอร์น้อยกว่าหรือเท่ากับ 0
                    else
                    {
                        // เคลียร์รายชื่อวิดีโอที่สามารถเล่นได้
                        fileIndex = string.Empty;
                    }
                }
                // ถ้าเออเร่อใด ๆ แสดงข้อความออกทาง Debug
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }

                // พักการทำงานของลูป 5 วินาที
                System.Threading.Thread.Sleep(5000);
            }
        }

        // อีเว้นทำงานเมื่อกดปุ่ม Process
        private void cmdProcess_Click(object sender, RoutedEventArgs e)
        {
            // ตรวจสอบข้อมูล ถ้าไม่มีวิดีโอในเพลลิส ให้ออกจากคำสั่ง
            if(!(listMedia.Items.Count > 0))
            {
                MessageBox.Show("ข้อผิดพลาดด้านวิดีโอ", "ไม่พบไฟล์วิดีโอใด ๆ ในเพลลิส !", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // ถ้าข้อความในปุ่ม Process เท่ากับ Process
            if (cmdProcess.Content.ToString() == "Process")
            {
                try
                {
                    // กำหนดชื่อวิดีโอให้เล่นวิดีโอจากตำแหน่งปัจจุบัน
                    fileIndex = listMedia.Items[playerIndex].ToString();

                    // กำหนดข้อความในปุ่ม Process เท่ากับ Cancle
                    cmdProcess.Content = "Cancle";

                    // กำหนดให้ตัวแปร path มีค่าเท่ากับโฟลเดอร์ปัจจุบันที่เลือก ตามด้วยชื่อวิดีโอที่อ่านได้จาก list
                    string path = txtPath.Text + "\\" + fileIndex;
                    // แสดงตำแหน่งวิดีโอที่เล่นใน Debug
                    Debug.WriteLine(path);
                    // ถ้า capture เท่ากับ null
                    if (capture == null)
                    {
                        // สร้างตัวเล่นวิดีโอตามตำแหน่งชื่อวิดีโอ
                        capture = new Capture(path);
                        capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_WIDTH, 1280);
                        capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT, 720);
                        // กำหนดค่าตัวแปรขนาด กว้าง , สูง ของวิดีโอ
                        VidX = capture.Width;
                        VidY = capture.Height;
                        // สร้างเธรดอัพเดทเฟรม
                        bgUpdateFrame = new System.Threading.Thread(CaptureUpdate);
                        // กำหนดให้เริ่มเล่นเฟรมแรก
                        videoFrameIndex = 0;
                        // กำหนดค่าจำนวนเฟรมทั้งหมดของวิดีโอ
                        videoFrameCount = (uint)Math.Floor(capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_COUNT));
                        // กำหนดสถานะว่าวิดีโอกำลังเล่นอยู่
                        isMediaEnded = false;
                        // เริ่มการทำงานของเธรด
                        bgUpdateFrame.Start();

                        Debug.WriteLine("Video Frame : {0}", videoFrameCount);
                    }
                    else
                    {
                        // คืนค่า Ram ให้ OS ก่อน
                        capture.Dispose();
                        capture = null;
                        GC.Collect();
                        // สร้างตัวเล่นวิดีโอตามตำแหน่งชื่อวิดีโอ
                        capture = new Capture(path);
                        capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_WIDTH, 1280);
                        capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT, 720);
                        // กำหนดค่าตัวแปรขนาด กว้าง , สูง ของวิดีโอ
                        VidX = capture.Width;
                        VidY = capture.Height;
                        // สร้างเธรดอัพเดทเฟรม
                        bgUpdateFrame = new System.Threading.Thread(CaptureUpdate);
                        // กำหนดให้เริ่มเล่นเฟรมแรก
                        videoFrameIndex = 0;
                        // กำหนดค่าจำนวนเฟรมทั้งหมดของวิดีโอ
                        videoFrameCount = (uint)Math.Floor(capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_COUNT));
                        // กำหนดสถานะว่าวิดีโอกำลังเล่นอยู่
                        isMediaEnded = false;
                        // เริ่มการทำงานของเธรด
                        bgUpdateFrame.Start();

                        Debug.WriteLine("Video Frame : {0}", videoFrameCount);
                    }
                }
                catch (Exception) { }
            }
            // ถ้าข้อความในปุ่ม Process ไม่เท่ากับ Process
            else
            {
                // กำหนดข้อความในปุ่ม Process เท่ากับ Process
                cmdProcess.Content = "Process";
                // ยกเลิกเธรดอัพเดทเฟรมก่อนหน้านี้
                bgUpdateFrame.Abort();
            }
        }

        // ฟังชั่นอ่านไฟล์จากโฟลเดอร์ (แล้วส่งค่ากลับเป็น list แบบ IEnumerable)
        private IEnumerable<string> GetFiles(string sourceDirectory, string[] exts, SearchOption searchOpt)
        {
            // ส่งค่ากลับโดยรับค่าจากโฟลเดอร์ที่เลือก และค่านามสกุลที่ตรงกันกับพารามิเตอร์
            return Directory.GetFiles(sourceDirectory, "*.*", searchOpt)
                    .Where(inS => exts.Contains(System.IO.Path.GetExtension(inS),
                           StringComparer.OrdinalIgnoreCase));
        }

        // ฟังชั่นรันสคริปไพธอน
        private void RunScriptPython()
        {
            try
            {
                // กำหนดตำแหน่งไฟล์ไพธอน
                string fileName = Environment.CurrentDirectory + @"\script\script.py";
                // สร้างโปรเซสขึ้นมา
                Process p = new Process();
                // กำหนดตำแหน่งไฟล์ไพธอนและรันสคริป
                p.StartInfo = new ProcessStartInfo(@"C:\Users\User\AppData\Local\Programs\Python\Python36\python.exe", fileName)
                {
                    RedirectStandardOutput = true, // แสดง outputs
                    UseShellExecute = false, // ไม่ใช้การประมวลผลผ่าน cmd
                    CreateNoWindow = true // ไม่แสดงหน้าต่างของ cmd
                };
                p.Start();

                // อ่าน output จากสคริป
                string output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();

                // แสดงใน Debug และ Log
                Debug.WriteLine(output);
                AddLog("ไพธอนสคริปทำงาน (output) : " + output);
            }
            // ถ้าเออเร่อขณะรันสคริป
            catch (Exception ex)
            {
                // แสดงข้อความว่าไม่พบโปรแกรมไพธอน
                AddLog("ไม่พบโปรแกรมไพธอน 3.6 ติดตั้งอยู่ !");
            }
        }

        // ฟังชั่นเพิ่มข้อความใน Log
        private void AddLog(string msg)
        {
            // ร้องขอการ Interrupt Thread ของ txtLogDevice
            txtLogDevice.Dispatcher.Invoke(new Action(() =>
            {
                // เพิ่มข้อความลงไปจากพารามิเตอร์ และขึ้นบรรทัดใหม่
                txtLogDevice.AppendText(msg + Environment.NewLine);
                // เลื่อน Scroll Bar ลงมาล่างสุด
                txtLogDevice.ScrollToEnd();
            }));
        }

        // เรียกใช้ gdi32 ในการเข้าถึงวัตถุของ GDI+
        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr o);

        // ฟังชั่นแปลง IImage เป็น BitmapSource สำหรับแสดงผลบน Image WPF
        public static BitmapSource ToBitmapSource(IImage image)
        {
            try
            {
                // กำหนดตัวแปรรูปภาพแบบ IDisposable
                using (System.Drawing.Bitmap source = image.Bitmap)
                {
                    // กำหนดค่าพ้อยเตอร์จากรูปภาพ
                    IntPtr ptr = source.GetHbitmap();
                    // กำหนดให้ตัวแปร BitmapSource มีค่าเท่ากับพ้อยเตอร์ของรูปภาพจากพารามิเตอร์
                    BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                        ptr,
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

                    // ลบวัตถุของพ้อยเตอร์ (คืน Ram ให้ OS)
                    DeleteObject(ptr);
                    // ส่งค่ากลับ
                    return bs;
                }
            }
            // ถ้าเออเร่อ ให้ส่งค่า null กลับ
            catch (Exception) { return null; }
        }

        private void scollLowH_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // ถ้าเปลี่ยนค่าของ scollLowH ให้ค่าตัวแปร LowH เปลี่ยนไปด้วย
            LowH = (int)scollLowH.Value;
            lblLowH.Content = "Low H : " + LowH;
        }

        private void scollLowS_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // ถ้าเปลี่ยนค่าของ scollLowS ให้ค่าตัวแปร LowS เปลี่ยนไปด้วย
            LowS = (int)scollLowS.Value;
            lblLowS.Content = "Low S : " + LowS;
        }

        private void scollLowV_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // ถ้าเปลี่ยนค่าของ scollLowV ให้ค่าตัวแปร LowV เปลี่ยนไปด้วย
            LowV = (int)scollLowV.Value;
            lblLowV.Content = "Low V : " + LowV;
        }

        private void scollHighH_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // ถ้าเปลี่ยนค่าของ scollHighH ให้ค่าตัวแปร HighH เปลี่ยนไปด้วย
            HighH = (int)scollHighH.Value;
            lblHighH.Content = "High H : " + HighH;
        }

        private void scollHighS_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // ถ้าเปลี่ยนค่าของ scollHighS ให้ค่าตัวแปร HighS เปลี่ยนไปด้วย
            HighS = (int)scollHighS.Value;
            lblHighS.Content = "High S : " + HighS;
        }

        private void scollHighV_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // ถ้าเปลี่ยนค่าของ scollHighV ให้ค่าตัวแปร HighV เปลี่ยนไปด้วย
            HighV = (int)scollHighV.Value;
            lblHighV.Content = "High V : " + HighV;
        }

        private void imgCapture_MouseDown(object sender, MouseButtonEventArgs e)
        {
            // ปุ่มกดบันทึกรูปภาพด้วยมือ
            try
            {
                this.Hide();
                StreamWindow stream = new StreamWindow();
                stream.Show();

                /* Interrupt ImageControl (หยุดการคอนโทรลข้ามเธรด)
                picMedia.Dispatcher.Invoke(() =>
                {
                    // กำหนดชื่อไฟล์
                    String filePath = Environment.CurrentDirectory + @"\Image\" + DateTime.Now.ToString("Dec (hh-MM-yyyy) HH.mm.ss") + ".jpg";
                    // กำหนดประเภทการ Encoder ว่าจะใช้นามสกุลเป็น Jpg
                    var encoder = new JpegBitmapEncoder();
                    // เพิ่มเฟรมที่จะบันทึกเข้าสู่ตัวแปร Encoder
                    encoder.Frames.Add(BitmapFrame.Create((BitmapSource)picMedia.Source));
                    // บันทึกไฟล์
                    using (FileStream stream = new FileStream(filePath, FileMode.Create))
                        encoder.Save(stream);

                    AddLog($"บันทึกรูปภาพไปที่ {filePath} สำเร็จ !");
                }); */
            }
            // ถ้าเกิดเหตุผิดพลาดขณะบันทึก ให้แจ้งผลผ่านทาง Diagnostic.Debug (System CLI)
            catch (Exception ex) { Debug.WriteLine("[imgCapture Failed] : " + ex.Message); }
        }

        private void imgSave_MouseDown(object sender, MouseButtonEventArgs e)
        {
            // สร้างตัวแปรบัฟเฟอร์ (ขอหน่วยความจำ Ram จาก OS)
            byte[] data = new byte[72];

            // เขียนบัฟเฟอร์ลงตัวแปร data
            Array.Copy(BitConverter.GetBytes(LowH), 0, data, 0, 4); // กำหนดให้ LowH อ้างอิง index ที่ 0 ขนาด 4 byte
            Array.Copy(BitConverter.GetBytes(LowS), 0, data, 4, 4); // กำหนดให้ LowS อ้างอิง index ที่ 4 ขนาด 4 byte
            Array.Copy(BitConverter.GetBytes(LowV), 0, data, 8, 4); // กำหนดให้ LowV อ้างอิง index ที่ 8 ขนาด 4 byte
            Array.Copy(BitConverter.GetBytes(HighH), 0, data, 12, 4); // กำหนดให้ HighH อ้างอิง index ที่ 12 ขนาด 4 byte
            Array.Copy(BitConverter.GetBytes(HighS), 0, data, 16, 4); // กำหนดให้ HighS อ้างอิง index ที่ 16 ขนาด 4 byte
            Array.Copy(BitConverter.GetBytes(HighV), 0, data, 20, 4); // กำหนดให้ HighV อ้างอิง index ที่ 20 ขนาด 4 byte
            Array.Copy(BitConverter.GetBytes(getLEDX), 0, data, 24, 8); // กำหนดให้ getLEDX อ้างอิง index ที่ 24 ขนาด 8 byte
            Array.Copy(BitConverter.GetBytes(getLEDY), 0, data, 32, 8); // กำหนดให้ getLEDY อ้างอิง index ที่ 32 ขนาด 8 byte
            Array.Copy(BitConverter.GetBytes(getRoadX1), 0, data, 40, 8); // กำหนดให้ getRoadX1 อ้างอิง index ที่ 40 ขนาด 8 byte
            Array.Copy(BitConverter.GetBytes(getRoadY1), 0, data, 48, 8); // กำหนดให้ getRoadY1 อ้างอิง index ที่ 48 ขนาด 8 byte
            Array.Copy(BitConverter.GetBytes(getRoadX2), 0, data, 56, 8); // กำหนดให้ getRoadX2 อ้างอิง index ที่ 56 ขนาด 8 byte
            Array.Copy(BitConverter.GetBytes(getRoadY2), 0, data, 64, 8); // กำหนดให้ getRoadY2 อ้างอิง index ที่ 64 ขนาด 8 byte

            // นำบัฟเฟอร์ที่ได้เขียนลงในไฟล์
            File.WriteAllBytes(Environment.CurrentDirectory + @"\script\config.tmp", data);
            // แสดงข้อความว่าบึนทึกสำเร็จ
            MessageBox.Show("บันทึกการตั้งค่าโปรแกรมเรียบร้อย !", "การตั้งค่า", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        // ทำงานเมื่อคลิกปุ่ม Load HSV
        private void cmdLoadSetting_Click(object sender, RoutedEventArgs e)
        {
            // สร้างตัวอ่านไฟล์
            Microsoft.Win32.OpenFileDialog fileSelection = new Microsoft.Win32.OpenFileDialog();
            // กรองไฟล์เฉพาะนามสกุลที่กำหนด
            fileSelection.Filter = "Image File (*.bmp; *.jpg; *.png) | *.bmp; *.jpg; *.png";

            // ถ้ากดปุ่ม OK
            if (fileSelection.ShowDialog() == true)
            {
                try
                {
                    // กำหนดตัวแปร image ให้อ่านรูปภาพที่เลือก
                    Image<Hsv, Byte> image = new Image<Hsv, byte>(fileSelection.FileName);

                    // เรียกใช้ฟังชั่นเคลียร์ค่า HSV ที่เก็บไว้ทั้งหมด
                    ResetHsv();

                    // อ่านแกน X ของรูปภาพทั้งหมด
                    for (int x = 0; x < image.Cols; x++)
                    {
                        // อ่านแกน Y ของรูปภาพทั้งหมด
                        for (int y = 0; y < image.Rows; y++)
                        {
                            // หาค่าต่ำสุดของ H
                            if (image[y, x].Hue < LowH)
                            {
                                LowH = (int)image[y, x].Hue;
                                scollLowH.Value = LowH;
                            }

                            // หาค่าสูงสุดของ H
                            if (image[y, x].Hue > HighH)
                            {
                                HighH = (int)image[y, x].Hue;
                                scollHighH.Value = HighH;
                            }

                            // หาค่าต่ำสุดของ S
                            if (image[y, x].Satuation < LowS)
                            {
                                LowS = (int)image[y, x].Satuation;
                                scollLowS.Value = LowS;
                            }

                            // หาค่าสูงสุดของ S
                            if (image[y, x].Satuation > HighS)
                            {
                                HighS = (int)image[y, x].Satuation;
                                scollHighS.Value = HighS;
                            }

                            // หาค่าต่ำสุดของ V
                            if (image[y, x].Value < LowV)
                            {
                                LowV = (int)image[y, x].Value;
                                scollLowV.Value = LowV;
                            }

                            // หาค่าสูงสุดของ V
                            if (image[y, x].Value > HighV)
                            {
                                HighV = (int)image[y, x].Value;
                                scollHighV.Value = HighV;
                            }
                        }
                    }

                    // แสดงข้อความใน Debug ว่าโหลดการตั้งค่าเสร็จแล้ว
                    Debug.WriteLine("Load config ok;");
                }
                // ถ้าเออเร่อ ให้แสดงข้อความใน Debug ว่าโหลดการตั้งค่าผิดพลาดสาเหตุเพราะอะไร
                catch (Exception ex) { Debug.WriteLine("Load config error; " + ex.Message); }
            }
            GC.Collect();
        }

        // ฟังชั่นเคลียร์ค่า HSV ทั้งหมด
        private void ResetHsv()
        {
            HighH = 0;
            HighS = 0;
            HighV = 0;
            LowH = 255;
            LowS = 255;
            LowV = 255;
        }

        // ฟังชั่นวาดกรอบบนรูปภาพ
        private Image<Bgr, byte> DrawGridRed(Image<Bgr, byte> imgControl)
        {
            // ถ้ารูปภาพเท่ากับ null หรือพ้อยเตอร์ของรูปเท่ากับ 0 ให้ออกจากฟังชั่น
            if (imgControl == null || imgControl.Ptr == IntPtr.Zero)
                return null;

            int count = 0;

            try
            {
                Image<Bgr, byte> img = imgControl; // สร้างตัวแปรเก็บรูปแยกขึ้นใหม่จากพารามิเตอร์ สำหรับอ่านเขียนได้ด้วย
                Image<Hsv, byte> imageResult = img.Convert<Hsv, Byte>(); // สร้างตัวแปรขึ้นมาให้เป็นสีประเภท HSV
                // คำนวนหาค่า hsv จากค่าตำสุด และค่าสูงสุดที่กำหนด
                Image<Gray, byte> imageSource = imageResult.InRange(new Hsv(LowH, LowS, LowV), new Hsv(HighH, HighS, HighV)).SmoothMedian(5);
                // กำหนด font ตัวหนังสือ
                MCvFont font = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_DUPLEX, 0.7, 0.7);
                // กำหนด Contour (ตัวแยกวัตถุ อาทิเช่น วงกลม สี่เหลี่ยม สามเหลี่ยม)
                Contour<Point> contours;
                using (MemStorage store_contour = new MemStorage())
                    // ค้นหา Contour ทั้งหมดในรูปภาพ
                    for (contours = imageSource.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_TREE); contours != null; contours = contours.HNext)
                    {
                        // ถ้าขนาด Contour ที่พบใหญ่กว่า 100 และเล็กกว่า 1000
                        if (contours.Area > 50 && contours.Area < 500)
                        {
                            // ถ้าตำแหน่งแกน X ของ Contour มากกว่าตำแหน่งกรอบแกน x ที่กำหนด และน้อยกว่ากรอบแกน x + 25
                            if (contours.BoundingRectangle.Left >= getLEDX && contours.BoundingRectangle.Left <= getLEDX + 25)
                            {
                                // ถ้าตำแหน่งแกน Y ของ Contour มากกว่าตำแหน่งของกรอบแกน y ที่กำหนด และน้อยกว่ากรอบแกน y + 45
                                if (contours.BoundingRectangle.Top >= getLEDY && contours.BoundingRectangle.Top <= getLEDY + 45)
                                {
                                    // วาดกรอบรอบ ๆ contour ด้วยสีแดง
                                    img.Draw(contours.BoundingRectangle, new Bgr(System.Drawing.Color.Red), 2);
                                    // ค้นหากรอบไฟแดงที่เป็นวงกลม
                                    MCvMoments m = contours.GetMoments();
                                    // กำหนดรัศมีแกน x,y
                                    double x = m.m10 / m.m00; x = Convert.ToInt16(x);
                                    double y = m.m01 / m.m00; y = Convert.ToInt16(y);
                                    // กำหนดตำแหน่ง center ของ contour
                                    System.Drawing.PointF central = new System.Drawing.PointF((float)x, (float)y);
                                    // กำหนดขอบเขตวงกลมและวาดกรอบ
                                    CircleF circle = new CircleF(central, 2);
                                    img.Draw(circle, new Bgr(255, 0, 0), -1);
                                    // วาดข้อความตำแหน่งแกน x , y ที่พบ contour เป็นตัวหนังสือสีขาว
                                    img.Draw("X : " + x.ToString() + ", Y :" + y.ToString() + " [RED]", ref font, new Point(10, 520), new Bgr(System.Drawing.Color.White));
                                    count++;
                                }
                            }
                        }
                    }

                // แสดงกรอบไฟแดง
                img.Draw(new System.Drawing.Rectangle((int)getLEDX, (int)getLEDY, 25, 45), new Bgr(0, 0, 255), 2);
                // แสดงกรอบเส้นถนน
                img.Draw(new System.Drawing.Rectangle((int)getRoadX1, (int)getRoadY1, (int)getRoadX2 - (int)getRoadX1, (int)getRoadY2 - (int)getRoadY1), new Bgr(0, 0, 255), 2);

                if (count > 0)
                    foundLED = true;
                else
                    foundLED = false;

                return img;
            }
            catch (Exception) { return null; }
        }

        // อีเว้นทำงานเมื่อขยับเมาส์บนวิดีโอ
        private void picMedia_MouseMove(object sender, MouseEventArgs e)
        {
            // กำหนดแกน x จากตำแหน่งที่คลิกเมาส์
            double x = (VidX / picMedia.Width) * Mouse.GetPosition(picMedia).X;
            // กำหนดแกน y จากตำแหน่งที่คลิกเมาส์ 
            double y = (VidY / picMedia.Height) * Mouse.GetPosition(picMedia).Y;

            // ถ้าเป็นโหมดกรอบไฟแดง ให้แสดงว่าโหมดกรอบไฟแดง และแกน x,y / ตำแหน่งเมาส์ปัจจุบัน x,y
            if (controlLED.IsChecked == true)
                lblStatus.Content = "[โหมดกรอบไฟแดง] X : " + getLEDX.ToString("0.00") + ", Y : " + getLEDY.ToString("0.00") + "  —   [Move X] : " + x.ToString("0.00") + ", [Move Y] : " + y.ToString("0.00");

            // ถ้าเป็นโหมดกรอบไฟแดง ให้แสดงว่าโหมดเส้นถนน และแกน xy1-xy2 / ตำแหน่งเมาส์ปัจจุบัน x,y
            if (controlRoad.IsChecked == true)
                lblStatus.Content = "[โหมดเส้นถนน] XY 1 : (" + getRoadX1.ToString("0.00") + "," + getRoadY1.ToString("0.00") + ") XY 2 : (" + getRoadX2.ToString("0.00") + "," + getRoadY2.ToString("0.00") + ")  —   [Move X" + stepRoad + " To] : " + x.ToString("0.00") + ", [Move Y" + stepRoad + " To] : " + y.ToString("0.00");
        }

        // อีเว้นทำงานเมื่อคลิกบนวิดีโอ
        private void picMedia_MouseDown(object sender, MouseButtonEventArgs e)
        {
            // ถ้าเลือกโหมดกรอบไฟแดง
            if (controlLED.IsChecked == true)
            {
                // กำหนดให้แกน x ของกรอบไฟแดง = ตำแหน่งที่เมาส์คลิก
                getLEDX = (VidX / picMedia.Width) * Mouse.GetPosition(picMedia).X;
                // กำหนดให้แกน y ของกรอบไฟแดง = ตำแหน่งที่เมาส์คลิก
                getLEDY = (VidY / picMedia.Height) * Mouse.GetPosition(picMedia).Y;
                // แสดงข้อความบน lblStatus ว่าคลิกที่ตำแหน่งไหน
                lblStatus.Content = "[โหมดกรอบไฟแดง] X : " + getLEDX.ToString("0.00") + ", Y : " + getLEDY.ToString("0.00") + " (คลิก)";
                return;
            }
            // ถ้าเลือกโหมดเส้นถนน
            if (controlRoad.IsChecked == true)
            {
                // ถ้าทำงานที่สเต็ปแรก
                if (stepRoad == 1)
                {
                    // กำหนดให้แกน x1 ของเส้นถนน = ตำแหน่งที่เมาส์คลิก
                    getRoadX1 = (VidX / picMedia.Width) * Mouse.GetPosition(picMedia).X;
                    // กำหนดให้แกน y1 ของเส้นถนน = ตำแหน่งที่เมาส์คลิก
                    getRoadY1 = (VidY / picMedia.Height) * Mouse.GetPosition(picMedia).Y;
                    // แสดงข้อความบน lblStatus ว่าคลิกที่ตำแหน่งไหน
                    lblStatus.Content = "[โหมดเส้นถนน] X1 : " + getRoadX1.ToString("0.00") + ", Y1 : " + getRoadY1.ToString("0.00") + " (คลิก)";
                    // เปลี่ยนเป็นสเต็ป 2
                    stepRoad++;
                    // ออกจากฟังชั่น
                    return;
                }

                // ถ้าทำงานที่สเต็ปสอง
                if (stepRoad == 2)
                {
                    // กำหนดให้แกน x2 ของเส้นถนน = ตำแหน่งที่เมาส์คลิก
                    getRoadX2 = (VidX / picMedia.Width) * Mouse.GetPosition(picMedia).X;
                    // กำหนดให้แกน y2 ของเส้นถนน = ตำแหน่งที่เมาส์คลิก
                    getRoadY2 = (VidY / picMedia.Height) * Mouse.GetPosition(picMedia).Y;
                    // แสดงข้อความบน lblStatus ว่าคลิกที่ตำแหน่งไหน
                    lblStatus.Content = "[โหมดเส้นถนน] X2 : " + getRoadX2.ToString("0.00") + ", Y2 : " + getRoadY2.ToString("0.00") + " (คลิก)";
                    // เปลี่ยนเป็นสเต็ป 1
                    stepRoad--;
                    // ออกจากฟังชั่น
                    return;
                }
            }
        }

        // คำนวนเส้นถนน
        private bool Process(Image<Bgr, byte> img)
        {
            Image<Bgr, byte> cameraImage = img; // โหลดรูปภาพ
            bool founder = false; // ตัวเช็คสถานะ

            // ตัวแปรเก็บค่าสีพื้นหลัง
            Image<Gray, float> backgroundImg = new Image<Gray, float>(cameraImage.Width, cameraImage.Height);
            Image<Gray, float> foregroundImg = new Image<Gray, float>(cameraImage.Width, cameraImage.Height);

            // แปลงค่าเป็นสีเทา
            foregroundImg = cameraImage.Convert<Gray, float>();

            // ถ้ารูปไม่เท่ากับ null
            if (cameraImage != null)
            {
                // เช็คจำนวนเฟรม
                if (frameCount == 0)
                {
                    // เปลี่ยนค่าพื้นหลังถ้าเป็นเฟรมแรก
                    backgroundImg = foregroundImg;
                    // หาค่าเฉลี่ยเพื่อให้รูปภาพสมดุล
                    CvInvoke.cvRunningAvg(foregroundImg, backgroundImg, 0.009, System.IntPtr.Zero);
                }
                // ถ้าเฟรมมากกว่า 0
                else
                {
                    // หาค่าเฉลี่ยเฟรม
                    CvInvoke.cvRunningAvg(foregroundImg, backgroundImg, 0.009, System.IntPtr.Zero);
                    CvInvoke.cvAbsDiff(backgroundImg, foregroundImg, foregroundImg);
                    // เพิ่มความสมูท เพื่อลดจุดขาวรบกวน
                    CvInvoke.cvSmooth(foregroundImg, foregroundImg, SMOOTH_TYPE.CV_GAUSSIAN, 13, 13, 3, 1);
                    // ตัดสีที่ไม่ต้องการออก (สีเทาของถนน) และคำนวนการเคลื่อนไหววัตถุ
                    foregroundImg = foregroundImg.ThresholdBinary(new Gray(100), new Gray(255));
                    // หาค่าสีดำ
                    Image<Gray, byte> imageSource = foregroundImg.InRange(new Gray(0), new Gray(100)).SmoothMedian(5);
                    // กำหนดกรอบวัตถุ
                    int car_total = 0;
                    Contour<Point> contours;
                    
                    using (MemStorage store_contour = new MemStorage())
                        // ค้นหา Contour ทั้งหมดในรูปภาพ (สีดำ)
                        for (contours = imageSource.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL); contours != null; contours = contours.HNext)
                        {
                            // ถ้าขนาด Contour ที่พบใหญ่กว่า 50 และเล็กกว่า 1000
                            if (contours.Area > 50 && contours.Area < 1200)
                            {
                                // ตรวจสอบตำแหน่งของ Contour (ตามกรอบที่วาดไว้)
                                if (contours.BoundingRectangle.Top < getRoadY2 && contours.BoundingRectangle.Top > getRoadY1)
                                {
                                    if (contours.BoundingRectangle.Left < getRoadX2 && contours.BoundingRectangle.Left > getRoadX1)
                                    {
                                        // วาดกรอบรอบ ๆ Contour ด้วยสีดำ
                                        foregroundImg.Draw(contours.BoundingRectangle, new Gray(33), 2);
                                        cameraImage.Draw(contours.BoundingRectangle, new Bgr(System.Drawing.Color.Red), 2);
                                        // ค้นหากรอบ Contour
                                        MCvMoments m = contours.GetMoments();
                                        // กำหนดรัศมีแกน x,y
                                        double x = m.m10 / m.m00; x = Convert.ToInt16(x);
                                        double y = m.m01 / m.m00; y = Convert.ToInt16(y);
                                        // กำหนดตำแหน่ง center ของ contour
                                        System.Drawing.PointF central = new System.Drawing.PointF((float)x, (float)y);
                                        // กำหนดขอบเขตวงกลมและวาดกรอบ
                                        CircleF circle = new CircleF(central, 2);
                                        foregroundImg.Draw(circle, new Gray(33), -1);
                                        cameraImage.Draw(circle, new Bgr(System.Drawing.Color.Red), -1);
                                        founder = true;
                                        car_total++;
                                    }
                                }
                            }
                        }

                    MCvFont font = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_DUPLEX, 0.7, 0.7);
                    cameraImage.Draw("Car Total : " + car_total, ref font, new Point(10, 520), new Bgr(System.Drawing.Color.White));

                    // กำหนดค่าจำนวนรถ
                    lblCarCount.Dispatcher.BeginInvoke((Action)(() => lblCarCount.Content = "จำนวนรถ : " + car_total));
                }

                // ถ้าไม่พบรถในกรอบ ให้ตั้งสถานะพร้อมถ่ายรูปหากพบรถในเฟรมต่อไป
                if (!founder)
                    hasCapture = false;

                // เพิ่มค่าเฟรม
                frameCount++;

                // แสดงเฟรมวิดีโอที่ได้บน picMedia (บนหน้าจอ)
                picMedia.Dispatcher.BeginInvoke((Action)(() => picMedia.Source = ToBitmapSource(cameraImage)));

                // วาดการ Detect ทางจอเล็กขวามือ
                picThreshold.Dispatcher.BeginInvoke((Action)(() => picThreshold.Source = ToBitmapSource(foregroundImg)));
            }

            // ส่งค่ากลับ
            return founder;
        }
    }
}
