from goprocam import GoProCamera
from goprocam import constants
gpCam = GoProCamera.GoPro()

print(gpCam.getStatus(constants.Status.Status,constants.Status.STATUS.Mode)) #Gets current mode status #เรียกสถานะปัจจุบัน

print(gpCam.infoCamera(constants.Camera.Name)) #Gets camera name #เรียกชื่อกล้อง

print(gpCam.getStatus(constants.Status.Status, constants.Status.STATUS.BatteryLevel)) #Gets battery level #ได้รับระดับแบตเตอรี

print(gpCam.getMediaInfo("file")) #Latest media taken filename #สื่อชื่อไฟล์ที่นำมาใช้ล่าสุด

gpCam.shoot_video(3)
#gpCam.take_photo(5)#Takes a photo in 5 seconds.#ถ่ายรูปใน 5 วินาที

gpCam.downloadLastMedia()
           
            
